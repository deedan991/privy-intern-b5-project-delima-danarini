package ucase

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/ucase/contract"
)

type JwtCustomClaims struct {
	Name string `json:"name"`
	jwt.StandardClaims
}

type JwtResp struct {
	Type      string `json:"type"`
	Token     string `json:"token"`
	ExpiresAt string `json:"expires_at"`
}

// convert unix to date
func UnixToDate(unix int64) string {
	return time.Unix(unix, 0).Format("2006-01-02T15:04:05-0700")
}

func GenerateToken() contract.UseCase {

	claims := &JwtCustomClaims{
		"wafiq",
		jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Minute * 120).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	jwtToken, err := token.SignedString([]byte("wafiq7601032006000001"))
	if err != nil {
		fmt.Println(err)
	}

	expiresDate := UnixToDate(claims.ExpiresAt)

	return &JwtResp{
		Type:      "Bearer",
		Token:     jwtToken,
		ExpiresAt: expiresDate,
	}
}

func (u *JwtResp) Serve(data *appctx.Data) appctx.Response {
	return *appctx.NewResponse().WithCode(consts.CodeSuccess).WithData(u).WithMessage("success")
}
