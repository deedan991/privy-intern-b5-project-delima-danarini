package middleware

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/hash"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/logger"
)

func ValidateSignature(r *http.Request, conf *appctx.Config) int {

	key := conf.App.ApiKey

	bytess, err := ioutil.ReadAll(r.Body)

	signature := r.Header.Get("Signature")
	timestamp := r.Header.Get("Timestamp")
	key_id := r.Header.Get("X-Api-Key-ID")
	method := r.Method

	msg := string(bytess[:])
	msg = strings.Replace(msg, " ", "", -1)
	msg = strings.Replace(msg, "\r", "", -1)
	msg = strings.Replace(msg, "\n", "", -1)

	md5 := hash.MD5Hash(msg)
	hmac_signature := timestamp + ":" + key_id + ":" + method + ":" + md5
	hmac := hash.Hmac256(hmac_signature, key)
	hmac64 := hmac[:64]
	expectedSignature := "#" + key_id + ":#" + hmac64
	expectedSignature = base64.StdEncoding.EncodeToString([]byte(expectedSignature))

	nameField := "request"

	if err != nil {
		return consts.CodeBadRequest
	}

	b, err := ioutil.ReadAll(r.Body)

	if err != nil {
		logger.Warn(fmt.Sprintf("[middleware] cannot read request body , error %s", err.Error()), logger.Any(nameField, string(b)))
		return consts.CodeBadRequest
	}

	if expectedSignature != signature {
		logger.Info(fmt.Sprintf("[middleware] signature invalid , signature %s is nivalid", signature), logger.Any(nameField, string(b)))
		return consts.CodeAuthFailure
	}

	fmt.Println("signature valid")
	logger.Info(fmt.Sprintf("[middleware] valid signature %s", signature), logger.Any(nameField, string(b)))

	return consts.CodeSuccess
}
