package middleware

import (
	"net/http"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/appctx"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/consts"
)

type MiddlewareFunc func(r *http.Request, conf *appctx.Config) int

func FilterFunc(conf *appctx.Config, r *http.Request, mfs []MiddlewareFunc) int {
	for _, mf := range mfs {
		if status := mf(r, conf); status != consts.CodeSuccess {
			return status
		}
	}
	return consts.CodeSuccess
}
