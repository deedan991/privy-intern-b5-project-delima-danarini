package consts

const (
	LogEventNameServiceTerminated = "ServiceTerminated"
	LogEventNameServiceStarting   = "ServiceStarting"
)
