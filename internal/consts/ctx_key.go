package consts

const (
	CtxUserCode = iota
	CtxUserEmail
	CtxUserPhone
	CtxIP
	CtxLang
	CtxUserInfo
)
