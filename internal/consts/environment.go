package consts

const (
	EnvProduction  = `prod`
	EnvStaging     = `stg`
	EnvDevelopment = `dev`
)
