package consts

const (
	HeaderLanguageKey     = `X-Lang`
	HeaderContentTypeKey  = `Content-Type`
	HeaderContentTypeJSON = `application/json`
)
