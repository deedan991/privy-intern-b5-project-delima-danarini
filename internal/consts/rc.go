package consts

const (
	CodeSuccess             = 200
	CodeBadRequest          = 400
	CodeAuthFailure         = 401
	CodeNotFound            = 404
	CodeRequestTimedOut     = 408
	CodeDuplicatedEntry     = 409
	CodeUnprocessableEntity = 422
	CodeReachMaxLimit       = 429
	CodeInternalServerError = 500
	CodeServerBusy          = 503
	CodeUnknownError        = 520
)

const (
	StatusSuccess = "success"
	StatusFailed  = "failed"
)
