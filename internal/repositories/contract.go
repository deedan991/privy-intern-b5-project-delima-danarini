package repositories

import (
	"context"
	"database/sql"
)

type DBTransaction interface {
	ExecTX(ctx context.Context, options *sql.TxOptions, fn func(context.Context, StoreTX) (int64, error)) (int64, error)
}

type StoreTX interface {
}
