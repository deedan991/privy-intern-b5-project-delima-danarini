package repositories

import (
	"context"

	"gitlab.privy.id/privypass/privypass-oauth2-core-se/internal/entity"
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/pkg/mariadb"
)

type User interface {
	Get(ctx context.Context) ([]entity.User, error)
}

type userImplementation struct {
	conn mariadb.Adapter
}

func NewUserImplementation(conn mariadb.Adapter) User {
	return &userImplementation{
		conn: conn,
	}
}

func (r *userImplementation) Get(ctx context.Context) (users []entity.User, err error) {
	query := "SELECT id, name FROM users"
	rows, err := r.conn.QueryRows(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var user entity.User

	for rows.Next() {
		var user entity.User
		err = rows.Scan(&user.ID, &user.Name)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
		err := rows.Scan(&user.ID, &user.Name)
		if err != nil {
			return nil, err
		}
	}

	users = append(users, user)

	return users, nil
}
