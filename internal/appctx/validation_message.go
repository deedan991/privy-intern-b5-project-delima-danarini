package appctx

type ValidationMessage map[string][]string

func NewValidationMessage() ValidationMessage {
	return ValidationMessage{}
}

func (v ValidationMessage) AddMessage(field string, msg ...string) ValidationMessage {
	_, ok := v[field]
	if !ok {
		v[field] = msg
	}
	return v
}
