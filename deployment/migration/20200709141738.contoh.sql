-- +goose Up
-- +goose StatementBegin
--please delete this migration cause for example purpose

CREATE TABLE IF NOT EXISTS example(
    id INT UNSIGNED AUTO_INCREMENT,
    name VARCHAR(100) DEFAULT NULL,
    email VARCHAR(10) NOT NULL,
    phone VARCHAR(20) DEFAULT '',
    PRIMARY KEY id(id),
    UNIQUE email(email)
)ENGINE = InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
-- +goose StatementEnd

INSERT INTO example (name, email, phone, address) VALUES ('John Doe','john.doe@gmail.com','082111110','Jl. Merdeka Raya');

-- +goose Down
-- +goose StatemenBegin

SELECT 'do nothing';
-- +goose StatementEnd